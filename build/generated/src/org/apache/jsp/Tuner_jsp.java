package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.sql.*;

public final class Tuner_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_out_value_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_out_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_out_value_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Tuner</title>\n");
      out.write("        \n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"Library\\AudioPlayerJS\\css\\audioplayer.css\"/>\n");
      out.write("\t\t\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"Library\\AudioPlayerJS\\css\\reset.css\"/>\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"Library\\AudioPlayerJS\\css\\demo.css\" />\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"Library\\CSS\\style.css\" media=\"screen\"/>\n");
      out.write("                <link rel=\"stylesheet\" href=\"Library\\CSS\\sidenav.css\" media=\"screen\"/>\n");
      out.write("                <link type =\"text/css\" rel=\"stylesheet\" href=\"bootstrap\\css\\bootstrap.min.css\"/>\n");
      out.write("                <link type =\"text/css\" rel=\"stylesheet\" href=\"bootstrap\\css\\bootstrap.css\"/>\n");
      out.write("                <link type =\"text/css\" rel=\"stylesheet\" href=\"bootstrap\\css\\bootstrap-theme.css\"/>\n");
      out.write("                <link type =\"text/css\" rel=\"stylesheet\" href=\"bootstrap\\css\\bootstrap-theme.min.css\"/>\n");
      out.write("                \n");
      out.write("                \n");
      out.write("                <link href=\"https://fonts.googleapis.com/css?family=Oswald\" rel=\"stylesheet\">\n");
      out.write("                <link href=\"https://fonts.googleapis.com/css?family=Roboto+Condensed\" rel=\"stylesheet\">\n");
      out.write("                <link href=\"https://fonts.googleapis.com/css?family=Montserrat\" rel=\"stylesheet\">\n");
      out.write("                <link href=\"https://fonts.googleapis.com/css?family=Fjalla+One\" rel=\"stylesheet\">\n");
      out.write("                <link href=\"https://fonts.googleapis.com/css?family=Montserrat+Alternates:900i\" rel=\"stylesheet\">\n");
      out.write("                \n");
      out.write("                <script src=\"Library\\JS\\sidenavjs.js\"></script>\n");
      out.write("\t\t<script src=\"jquery-3.1.1.js\"></script>\n");
      out.write("\t\t<script src=\"Library\\AudioPlayerJS\\js\\audioplayer.js\"></script>\n");
      out.write("                \n");
      out.write("\t\t<script>$( function() { $( 'audio' ).audioPlayer(); } );</script>\n");
      out.write("                \n");
      out.write("                <style>\n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                .mainmenu a:hover {\n");
      out.write("                        color:orange;\n");
      out.write("                    }\n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    .submenu .genreitems:hover {\n");
      out.write("                        color:#80ffaa;\n");
      out.write("                    }\n");
      out.write("                   \n");
      out.write("                    \n");
      out.write("                    .mainmenu a{\n");
      out.write("                        color: grey;\n");
      out.write("                    }\n");
      out.write("                    \n");
      out.write("                    .downloadbtn span:hover\n");
      out.write("                    {\n");
      out.write("                        color:#ff5050;\n");
      out.write("                    }\n");
      out.write("\n");
      out.write("                    </style>\n");
      out.write("                \n");
      out.write("                ");

                    ResultSet rs,rs1;
                    Connection con=null;
                    String Name,Img,Path,Album,Artist,Genre;
                    int sid;
                    try
                    {
                        Class.forName("com.mysql.jdbc.Driver");
            
                    }
                    catch(ClassNotFoundException ex)
                    {
                        System.out.println(ex.toString());
                    }
                    try
                    {
                        con=DriverManager.getConnection("jdbc:mysql://localhost:3306/onlinemusic","root","cms@123");
            
            
                    }
                    catch(SQLException e)
                    {
                        System.out.println(e.toString());
                    }
                    
                    
      out.write("\n");
      out.write("                            \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("        <div id=\"mySidenav\" class=\"sidenav\">\n");
      out.write("            <ul class=\"mainmenu\">\n");
      out.write("                <li><a href=\"Tuner.jsp\" style=\"text-decoration: none;font-size:30px;font-family:'Fjalla One', sans-serif;font-weight: bold;\"><span class=\"glyphicon glyphicon-home\"></span>&nbsp;&nbsp;&nbsp;Home</a></li>\n");
      out.write("                <br>\n");
      out.write("            <li><a href=\"#\" style=\"text-decoration: none;font-size:30px;font-family:'Fjalla One', sans-serif;font-weight: bold;\"><span class=\"glyphicon glyphicon-cd\"></span>&nbsp;&nbsp;&nbsp;Genre</a>\n");
      out.write("                <br>\n");
      out.write("                \n");
      out.write("                <ul class=\"submenu\">\n");
      out.write("                    ");

                        try
                        {   String sgenre;
                            PreparedStatement psgenre=con.prepareStatement("select DISTINCT SGenre from song");
                            rs1=psgenre.executeQuery();
                            while(rs1.next())
                            {
                                sgenre=rs1.getString(1);
                            
                    
      out.write("\n");
      out.write("                    <li><a href=\"");
      out.print(sgenre);
      out.write(".jsp\" class=\"genreitems\" style=\"text-decoration: none;background-color:#111;font-size:23px;font-family:'Fjalla One', sans-serif;font-weight: lighter;\"><span class=\"glyphicon glyphicon-record\"></span>&nbsp;&nbsp;&nbsp;");
      out.print(sgenre);
      out.write("</a></li>\n");
      out.write("                    ");

                        }

                        }
                        catch(Exception r)
                        {
                            System.out.println(r.toString());
                        
                        }
                    
      out.write("\n");
      out.write("                </ul>\n");
      out.write("                \n");
      out.write("            </li>\n");
      out.write("            <br>\n");
      out.write("            <li><a href=\"#\" style=\"text-decoration: none;font-size:30px;font-family:'Fjalla One', sans-serif;font-weight: bold;\"><span class=\"glyphicon glyphicon-earphone\"></span>&nbsp;&nbsp;&nbsp;Contact Us</a></li>\n");
      out.write("            <br>\n");
      out.write("            \n");
      out.write("            \n");
      out.write("            </ul>\n");
      out.write("            <span id=\"menubtnclose\" style=\"font-size:40px;color:orange;\" class=\"glyphicon glyphicon-chevron-left\" onclick=\"closeNav()\"></span>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        <span id=\"menubtn\" style=\"font-size:40px;color:orange;\" class=\"glyphicon glyphicon-chevron-right\" onclick=\"openNav()\"></span>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("        <div id=\"main\" class=\"container-fluid\" align=\"center\">\n");
      out.write("            \n");
      out.write("            <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n");
      out.write("                <div class=\"container-fluid\">\n");
      out.write("                    <div class=\"navbar-header\">\n");
      out.write("                        <a class=\"navbar-brand\" href=\"Tuner.jsp\" style=\"position: absolute;bottom:8px\"><b style=\"font-size:30px;color:orange;font-family: 'Montserrat Alternates', sans-serif; \"><span class=\"glyphicon glyphicon-headphones\"></span>&nbsp;Tuner</b></a>\n");
      out.write("                    </div>\n");
      out.write("                <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                    <li><a href=\"#\" ><span class=\"glyphicon glyphicon-user\"></span> Sign Up</a></li>\n");
      out.write("                    <li><a href=\"#\" ><span class=\"glyphicon glyphicon-log-in\"></span> Login</a></li>\n");
      out.write("                </ul>\n");
      out.write("                <form class=\"navbar-form navbar-center\">    \n");
      out.write("                <div class=\"input-group\">\n");
      out.write("                    <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\n");
      out.write("                    <div class=\"input-group-btn\">\n");
      out.write("                        <button class=\"btn btn-default\" type=\"submit\">\n");
      out.write("                            <i class=\"glyphicon glyphicon-search\"></i>\n");
      out.write("                        </button>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                </form>\n");
      out.write("                </div>\n");
      out.write("            </nav>\n");
      out.write("        \n");
      out.write("            ");
          try
                    {
                        PreparedStatement ps=con.prepareStatement("select SID,SName,SIMG,SPath,SArtist,SAlbum,SGenre from song");
                        rs=ps.executeQuery();
                        while(rs.next())
                        {
                            sid=rs.getInt(1);
                            Name=rs.getString(2);
                            Img=rs.getString(3);
                            Path=rs.getString(4);
                            Artist=rs.getString(5);
                            Album=rs.getString(6);
                            Genre=rs.getString(7);
      out.write("\n");
      out.write("                            <br><br>\n");
      out.write("\t\t\t\t<br>\n");
      out.write("                                <br>\n");
      out.write("                                <div  class=\"main\">\n");
      out.write("                                <div class=\"row\">\n");
      out.write("                                    <div class=\"col-lg-16 col-md-16 col-sm-16 col-xs-16\">\n");
      out.write("\t\t\t\t<div class=\"audio-player\" style=\"width:60%;height:270px;position:relative;box-shadow:5px 5px 20px black\">\n");
      out.write("                                    <div class=\"container\">\n");
      out.write("                                        <div id=\"metadiv\" class=\"row\" style=\"background:#eff5f5;position: absolute;width:72%;height:232px;left:28%\">\n");
      out.write("                                        \n");
      out.write("                                        <div class=\"col-lg-16 col-md-16 col-sm-16 col-xs-16\">\n");
      out.write("                                            \n");
      out.write("                                        \n");
      out.write("                                    <p id=\"titlesong\" style=\"font-family:'Fjalla One', sans-serif;font-weight: bold;font-size: 40px;color:#2d8659;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:10%;left:15%;\">");
      //  c:out
      org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_0 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
      _jspx_th_c_out_0.setPageContext(_jspx_page_context);
      _jspx_th_c_out_0.setParent(null);
      _jspx_th_c_out_0.setValue(Name);
      int _jspx_eval_c_out_0 = _jspx_th_c_out_0.doStartTag();
      if (_jspx_th_c_out_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
        return;
      }
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
      out.write("</p>\n");
      out.write("                                    <p id=\"sartist\" style=\"font-family: 'Fjalla One', sans-serif;font-size: 30px;color: #2d5986;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:35%;left:15%;\">");
      //  c:out
      org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_1 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
      _jspx_th_c_out_1.setPageContext(_jspx_page_context);
      _jspx_th_c_out_1.setParent(null);
      _jspx_th_c_out_1.setValue(Artist);
      int _jspx_eval_c_out_1 = _jspx_th_c_out_1.doStartTag();
      if (_jspx_th_c_out_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_1);
        return;
      }
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_1);
      out.write("</p>\n");
      out.write("                                    <p id=\"salbum\" style=\"font-family: 'Fjalla One', sans-serif;font-size: 30px;color: #ff4d4d;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:55%;left:15%;\">");
      //  c:out
      org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_2 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
      _jspx_th_c_out_2.setPageContext(_jspx_page_context);
      _jspx_th_c_out_2.setParent(null);
      _jspx_th_c_out_2.setValue(Album);
      int _jspx_eval_c_out_2 = _jspx_th_c_out_2.doStartTag();
      if (_jspx_th_c_out_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_2);
        return;
      }
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_2);
      out.write("</p>\n");
      out.write("                                    <a href=\"\"><p id=\"genre\" style=\"font-family: 'Oswald', sans-serif;font-size: 22px;color:#990099;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:75%;left:15%\">");
      //  c:out
      org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_3 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
      _jspx_th_c_out_3.setPageContext(_jspx_page_context);
      _jspx_th_c_out_3.setParent(null);
      _jspx_th_c_out_3.setValue(Genre);
      int _jspx_eval_c_out_3 = _jspx_th_c_out_3.doStartTag();
      if (_jspx_th_c_out_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_3);
        return;
      }
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_3);
      out.write("</p></a>\n");
      out.write("                                           \n");
      out.write("                                    <a  class=\"downloadbtn\" href=\"");
      out.print(Path);
      out.write("\" style=\"position:absolute;top:12%;left:95%;color:#009999\" download><span class=\"glyphicon glyphicon-download-alt\" style=\"font-size:25px\"></span></a>\n");
      out.write("                                        </div>\n");
      out.write("                                    \n");
      out.write("                                        </div>\n");
      out.write("                                    \n");
      out.write("                                    </div>\n");
      out.write("                                        <div id=\"simg\" class=\"container\">\n");
      out.write("                                    <img class=\"cover\" src=\"");
      out.print(Img);
      out.write("\" alt=\"\" width=28% height=100% style=\"position:absolute;top:0px;left:0px;\">\n");
      out.write("                                    </div>\n");
      out.write("\t\t\t\t<div style=\"width:72%;position: absolute;top:86%;left:28%;\">\n");
      out.write("                                   \n");
      out.write("                                    <audio id=\"");
      out.print(sid);
      out.write("\" preload=\"auto\" controls onplay=\"hits(x)\">\n");
      out.write("                                            <source src=\"");
      out.print(Path);
      out.write("\">\n");
      out.write("\t\t\t\t\t</audio>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                                </div>\n");
      out.write("                                </div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                      \n");
      out.write("               \n");
      out.write("            <br>\n");
      out.write("                            \n");
      out.write("                            ");

                                
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.toString());
                    }
                
      out.write("\n");
      out.write("                \n");
      out.write("         <footer style=\"width: 100%;height:200px;background-color:rgba(0, 0, 0, 0.5);\">\n");
      out.write("            <p style=\"font-family: Roboto Condensed; color:#33ffad;font-size:30px;\"><b>© Copyright 2017 Tuner</b></p>\n");
      out.write("            <p style= \"font-family: Roboto Condensed;color:#ffff99;font-size:20px\">Disclaimer: The contents of this platform are the sole property of the various contributing Record Labels.</p>\n");
      out.write("            <p><a href=\"#\" style= \"font-family: Roboto Condensed;color:orange;font-size:20px\">About Us</a></p>\n");
      out.write("            <p><a href=\"#\"  style= \"font-family: Roboto Condensed;color:orange;font-size:20px\">Contact Us</a></p>\n");
      out.write("        </footer>\n");
      out.write("                \n");
      out.write("        </div>\n");
      out.write("            \n");
      out.write("        \n");
      out.write("      \n");
      out.write("        <script src=\"bootstrap\\js\\bootstrap.min.js\"></script>  \n");
      out.write("        <script src=\"bootstrap\\js\\bootstrap.js\"></script> \n");
      out.write("        <script src=\"bootstrap\\js\\npm.js\"></script> \n");
      out.write("        \n");
      out.write("        \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
