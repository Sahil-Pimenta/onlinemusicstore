<%-- 
    Document   : Tuner
    Created on : Apr 3, 2017, 6:30:41 PM
    Author     : Sahil
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*" import="java.sql.*"%>
<%@taglib uri="http://java.sun.com/jstl/sql_rt" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tuner</title>
        
		<link rel="stylesheet" href="Library\AudioPlayerJS\css\audioplayer.css"/>
		
		<link rel="stylesheet" href="Library\AudioPlayerJS\css\reset.css"/>
		<link rel="stylesheet" href="Library\AudioPlayerJS\css\demo.css" />
		<link rel="stylesheet" href="Library\CSS\style.css" media="screen"/>
                <link rel="stylesheet" href="Library\CSS\sidenav.css" media="screen"/>
                <link type ="text/css" rel="stylesheet" href="bootstrap\css\bootstrap.min.css"/>
                <link type ="text/css" rel="stylesheet" href="bootstrap\css\bootstrap.css"/>
                <link type ="text/css" rel="stylesheet" href="bootstrap\css\bootstrap-theme.css"/>
                <link type ="text/css" rel="stylesheet" href="bootstrap\css\bootstrap-theme.min.css"/>
                
                
                <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:900i" rel="stylesheet">
                
                <script src="Library\JS\sidenavjs.js"></script>
		<script src="jquery-3.1.1.js"></script>
		<script src="Library\AudioPlayerJS\js\audioplayer.js"></script>
                
		<script>$( function() { $( 'audio' ).audioPlayer(); } );</script>
                
                <style>
                    
                    
                    
                .mainmenu a:hover {
                        color:orange;
                    }
                    
                    
                    .submenu .genreitems:hover {
                        color:#80ffaa;
                    }
                   
                    
                    .mainmenu a{
                        color: grey;
                    }
                    
                    .downloadbtn span:hover
                    {
                        color:#ff5050;
                    }

                    </style>
                
                <%
                    ResultSet rs,rs1;
                    Connection con=null;
                    String Name,Img,Path,Album,Artist,Genre;
                    int sid;
                    try
                    {
                        Class.forName("com.mysql.jdbc.Driver");
            
                    }
                    catch(ClassNotFoundException ex)
                    {
                        System.out.println(ex.toString());
                    }
                    try
                    {
                        con=DriverManager.getConnection("jdbc:mysql://localhost:3306/onlinemusic","root","cms@123");
            
            
                    }
                    catch(SQLException e)
                    {
                        System.out.println(e.toString());
                    }
                    
                    %>
                            
    </head>
    <body>
        
        <div id="mySidenav" class="sidenav">
            <ul class="mainmenu">
                <li><a href="Tuner.jsp" style="text-decoration: none;font-size:30px;font-family:'Fjalla One', sans-serif;font-weight: bold;"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Home</a></li>
                <br>
            <li><a href="#" style="text-decoration: none;font-size:30px;font-family:'Fjalla One', sans-serif;font-weight: bold;"><span class="glyphicon glyphicon-cd"></span>&nbsp;&nbsp;&nbsp;Genre</a>
                <br>
                
                <ul class="submenu">
                    <%
                        try
                        {   String sgenre;
                            PreparedStatement psgenre=con.prepareStatement("select DISTINCT SGenre from song");
                            rs1=psgenre.executeQuery();
                            while(rs1.next())
                            {
                                sgenre=rs1.getString(1);
                            
                    %>
                    <li><a href="<%=sgenre%>.jsp" class="genreitems" style="text-decoration: none;background-color:#111;font-size:23px;font-family:'Fjalla One', sans-serif;font-weight: lighter;"><span class="glyphicon glyphicon-record"></span>&nbsp;&nbsp;&nbsp;<%=sgenre%></a></li>
                    <%
                        }

                        }
                        catch(Exception r)
                        {
                            System.out.println(r.toString());
                        
                        }
                    %>
                </ul>
                
            </li>
            <br>
            <li><a href="#" style="text-decoration: none;font-size:30px;font-family:'Fjalla One', sans-serif;font-weight: bold;"><span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;&nbsp;Contact Us</a></li>
            <br>
            
            
            </ul>
            <span id="menubtnclose" style="font-size:40px;color:orange;" class="glyphicon glyphicon-chevron-left" onclick="closeNav()"></span>
        </div>
        
        <span id="menubtn" style="font-size:40px;color:orange;" class="glyphicon glyphicon-chevron-right" onclick="openNav()"></span>
        
        
        <div id="main" class="container-fluid" align="center">
            
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="Tuner.jsp" style="position: absolute;bottom:8px"><b style="font-size:30px;color:orange;font-family: 'Montserrat Alternates', sans-serif; "><span class="glyphicon glyphicon-headphones"></span>&nbsp;Tuner</b></a>
                    </div>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" ><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#" ><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
                <form class="navbar-form navbar-center">    
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
                </form>
                </div>
            </nav>
        
            <%          try
                    {
                        PreparedStatement ps=con.prepareStatement("select SID,SName,SIMG,SPath,SArtist,SAlbum,SGenre from song");
                        rs=ps.executeQuery();
                        while(rs.next())
                        {
                            sid=rs.getInt(1);
                            Name=rs.getString(2);
                            Img=rs.getString(3);
                            Path=rs.getString(4);
                            Artist=rs.getString(5);
                            Album=rs.getString(6);
                            Genre=rs.getString(7);%>
                            <br><br>
				<br>
                                <br>
                                <div  class="main">
                                <div class="row">
                                    <div class="col-lg-16 col-md-16 col-sm-16 col-xs-16">
				<div class="audio-player" style="width:60%;height:270px;position:relative;box-shadow:5px 5px 20px black">
                                    <div class="container">
                                        <div id="metadiv" class="row" style="background:#eff5f5;position: absolute;width:72%;height:232px;left:28%">
                                        
                                        <div class="col-lg-16 col-md-16 col-sm-16 col-xs-16">
                                            
                                        
                                    <p id="titlesong" style="font-family:'Fjalla One', sans-serif;font-weight: bold;font-size: 40px;color:#2d8659;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:10%;left:15%;"><c:out value="<%=Name%>"/></p>
                                    <p id="sartist" style="font-family: 'Fjalla One', sans-serif;font-size: 30px;color: #2d5986;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:35%;left:15%;"><c:out value="<%=Artist%>"/></p>
                                    <p id="salbum" style="font-family: 'Fjalla One', sans-serif;font-size: 30px;color: #ff4d4d;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:55%;left:15%;"><c:out value="<%=Album%>"/></p>
                                    <a href=""><p id="genre" style="font-family: 'Oswald', sans-serif;font-size: 22px;color:#990099;text-shadow: 1px 1px 1px rgba(0,0,0, .5);position: absolute;top:75%;left:15%"><c:out value="<%=Genre%>"/></p></a>
                                           
                                    <a  class="downloadbtn" href="<%=Path%>" style="position:absolute;top:12%;left:95%;color:#009999" download><span class="glyphicon glyphicon-download-alt" style="font-size:25px"></span></a>
                                        </div>
                                    
                                        </div>
                                    
                                    </div>
                                        <div id="simg" class="container">
                                    <img class="cover" src="<%=Img%>" alt="" width=28% height=100% style="position:absolute;top:0px;left:0px;">
                                    </div>
				<div style="width:72%;position: absolute;top:86%;left:28%;">
                                   
                                    <audio id="<%=sid%>" preload="auto" controls onplay="hits(x)">
                                            <source src="<%=Path%>">
					</audio>
				</div>
				</div>
                                </div>
                                </div>
				</div>
                      
               
            <br>
                            
                            <%
                                
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.toString());
                    }
                %>
                
         <footer style="width: 100%;height:200px;background-color:rgba(0, 0, 0, 0.5);">
            <p style="font-family: Roboto Condensed; color:#33ffad;font-size:30px;"><b>© Copyright 2017 Tuner</b></p>
            <p style= "font-family: Roboto Condensed;color:#ffff99;font-size:20px">Disclaimer: The contents of this platform are the sole property of the various contributing Record Labels.</p>
            <p><a href="#" style= "font-family: Roboto Condensed;color:orange;font-size:20px">About Us</a></p>
            <p><a href="#"  style= "font-family: Roboto Condensed;color:orange;font-size:20px">Contact Us</a></p>
        </footer>
                
        </div>
            
        
      
        <script src="bootstrap\js\bootstrap.min.js"></script>  
        <script src="bootstrap\js\bootstrap.js"></script> 
        <script src="bootstrap\js\npm.js"></script> 
        
        
    </body>
</html>
